/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package se.nackademin;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author mattias
 */
public class Translator {
        
    private List<String> adjektiv = new ArrayList<>();
    private List<String> verb = new ArrayList<>();
    private List<String> substantiv = new ArrayList<>();
        
    public void methodFillLists(){
        adjektiv.add("stor");
        adjektiv.add("liten");
        adjektiv.add("stark");
        adjektiv.add("svag");
        adjektiv.add("mjuk");
        adjektiv.add("hård");
        adjektiv.add("snabb");
        adjektiv.add("vacker");
        adjektiv.add("ljus");
        adjektiv.add("mörk");
        
        verb.add("springa");
        verb.add("ljuga");
        verb.add("flyga");
        verb.add("se");
        verb.add("vara");
        verb.add("äta");
        verb.add("mäta");
        verb.add("gå");
        verb.add("röra");
        verb.add("resa");
        
        substantiv.add("en lönehöjning");
        substantiv.add("en lönesänkning");
        substantiv.add("en fotboja");
        substantiv.add("en katt");
        substantiv.add("en hund");
        substantiv.add("ett hus");
        substantiv.add("ett barn");
        substantiv.add("ett elstängsel");
        substantiv.add("en dator");
        substantiv.add("ett golv");
    }
        
    public String getVerb(int i){
        
        return verb.get(i);
    }
    
    public String getSubstantiv(int i){
        
        return substantiv.get(i);
    }
    
    public String getAdjektiv(int i){
        
        return adjektiv.get(i);
    }
}
