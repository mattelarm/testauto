package se.nackademin;

public class MagicNumbers {

    private String name;
    private int income;
    private String location;
    private int age;
    private int height;

    public int calculateA() {
        int temp = name.length() - name.replaceAll(" ", "").length();
        int temp2 = temp +1;
        int temp3 = temp2 + age;
            
        while(temp3 >10){
            temp3 -=7;
            
        }
        
        return temp3;
    }

    public int calculateB() {
        int temp = location.length()+income;
         
        while(temp >=10){
            temp -=7;
        }
        return temp;
    }

    public int calculateC() {
        int temp = calculateA();
        int temp2 = calculateB();
        int temp3 = temp + temp2;
        int temp4 = temp3 - height;
        
        while (temp4 <0){
            temp4 +=10;
        }
        return temp4;
    }

    public int calculateD() {
        int temp = calculateA();
        
        if(temp >5){
            temp += calculateB();
        }else {
            temp += calculateC();
        }
        
        temp -= income;
        while(temp <0){
            temp +=10;
        }
        
        return temp;
    }

    public int calculateE() {
        double temp = age;
        double temp2 = temp * income; 
        double temp3 = temp2 * income;
        double temp4 = temp3 * height;
        double temp5 = Math.sqrt(temp4);
        
        while(temp5 >10){
            temp5 /=2;
        }
        
        long temp6 = Math.round(temp5);
        int temp7 = (int)temp6;
        return temp7;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setIncome(int income) {
        this.income = income;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setHeight(int height) {
        this.height = height;
    }
}
